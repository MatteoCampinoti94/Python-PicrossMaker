import sys, os
import re
import random

if len(sys.argv) > 1 and sys.argv[1] in ('-h', '--help'):
    print('To specify size use Column*Rows argument (CxR, CxR and C*R are accepted)')
    print('To specify name simply use the name as argument')


size     = [10, 10]
size_re  = re.compile('^([0-9]+)[xX*]([0-9]+)$')
fill     = .3
fill_re  = re.compile('^([0-9]*\.[0-9]+)$')
name     = 'Picross'
def_cell = '#'
def_null = ' '


if len(sys.argv) > 1:
    for arg in sys.argv[1:]:
        if size_re.match(arg):
            size = size_re.match(arg)
            size = [int(size.group(1)), int(size.group(2))]
        elif fill_re.match(arg):
            fill = fill_re.match(arg).group(1)
            fill = float(fill)
        else:
            name = arg

if fill >= 1 or fill <= 0:
    sys.exit(1)
if size[0] == 1 or size[1] == 1:
    sys.exit(2)

########################################

if fill <= 0.5:
    picross = [[def_null for i in range(0, size[0])] for j in range(0, size[1])]

    fill_t = int(fill * (size[0]*size[1]))
    fill_i = 0
    print(f'\r{(fill_i/fill)*100:0.2f}%', end='', flush='')
    while fill_i < fill_t:
        cell = (random.randint(0, size[1]-1), random.randint(0, size[0]-1))
        if picross[cell[0]][cell[1]] == def_null:
            picross[cell[0]][cell[1]] = def_cell
            fill_i += 1
        print(f'\r{(fill_i/fill)*100:0.2f}%', end='', flush='')
    print('\b \b'*os.get_terminal_size()[0], end='\r', flush='')
else:
    picross = [[def_cell for i in range(0, size[0])] for j in range(0, size[1])]

    fill_t = int((1-fill) * (size[0]*size[1]))
    fill_i = 0
    print(f'\r{(fill_i/fill)*100:0.2f}%', end='', flush='')
    while fill_i < fill_t:
        cell = (random.randint(0, size[1]-1), random.randint(0, size[0]-1))
        if picross[cell[0]][cell[1]] == def_cell:
            picross[cell[0]][cell[1]] = def_null
            fill_i += 1
        print(f'\r{(fill_i/fill)*100:0.2f}%', end='', flush='')
    print('\b \b'*os.get_terminal_size()[0], end='\r', flush='')

########################################

picross_cols_str = []
for i in range(0, size[0]):
    col = ''
    for row in picross:
        col += row[i]
    picross_cols_str.append(col)

picross_rows_str = [''.join(row) for row in picross]

########################################

picross_cols = ['']*size[0]
picross_rows = ['']*size[1]
for i in range(0, size[0]):
    col = [c for c in picross_cols_str[i].split(def_null) if c]
    col = [len(c) for c in col] if col else [0]
    picross_cols[i] = col

for j in range(0, size[1]):
    row = [r for r in picross_rows_str[j].split(def_null) if r]
    row = [len(r) for r in row] if row else [0]
    picross_rows[j] = row

########################################

max_hint_cols_n = 0
max_hint_cols_w = 0
for i in range(0, len(picross_cols)):
    col = picross_cols[i]

    if len(col) > max_hint_cols_n:
        max_hint_cols_n = len(col)
    for c in col:
        if len(str(c)) > max_hint_cols_w:
            max_hint_cols_w = len(str(c))

max_hint_rows_n = 0
max_hint_rows_w = 0
for i in range(0, len(picross_rows)):
    row = picross_rows[i]

    if len(row) > max_hint_rows_n:
        max_hint_rows_n = len(row)
    for c in row:
        if len(str(c)) > max_hint_rows_w:
            max_hint_rows_w = len(str(c))

picross_cols = [[' ']*(max_hint_cols_n-len(col)) + col for col in picross_cols]
picross_rows = [[' ']*(max_hint_rows_n-len(row)) + row for row in picross_rows]

########################################

with open(name+'_solution.txt', 'w') as f:
    f.write(f'Size: {size[0]}x{size[1]}\nFill: {fill*100:0.2f}%\nCell on : "{def_cell}"\nCell off: "{def_null}"\n\n')
    for i in range(0, max_hint_cols_n):
        f.write((' '*max_hint_rows_w + ' ')*max_hint_rows_n)
        f.write('|')
        for col in picross_cols:
            f.write(' ')
            f.write(' '*(max_hint_cols_w-len(str(col[i]))))
            f.write(str(col[i]))
        f.write('\n')
    f.write(('-'*max_hint_rows_w + '-')*max_hint_rows_n)
    f.write('+')
    f.write(('-'*max_hint_cols_w + '-')*size[0])
    f.write('\n')
    for i in range(0, size[1]):
        hint = picross_rows[i]
        for j in range(0, max_hint_rows_n):
            f.write(str(hint[j]))
            f.write(' '*(max_hint_rows_w-len(str(hint[j]))) + ' ')
        f.write('|')
        for cell in picross[i]:
            f.write(' '*(max_hint_cols_w-len(str(cell))) + ' ')
            f.write(cell)
        f.write('\n')

with open(name+'.txt', 'w') as f:
    f.write(f'Size: {size[0]}x{size[1]}\nFill: {fill*100:0.2f}%\nCell on : "{def_cell}"\nCell off: "{def_null}"\n\n')
    for i in range(0, max_hint_cols_n):
        f.write((' '*max_hint_rows_w + ' ')*max_hint_rows_n)
        f.write('|')
        for col in picross_cols:
            f.write(' ')
            f.write(' '*(max_hint_cols_w-len(str(col[i]))))
            f.write(str(col[i]))
        f.write('\n')
    f.write(('-'*max_hint_rows_w + '-')*max_hint_rows_n)
    f.write('+')
    f.write(('-'*max_hint_cols_w + '-')*size[0])
    f.write('\n')
    for i in range(0, size[1]):
        hint = picross_rows[i]
        for j in range(0, max_hint_rows_n):
            f.write(str(hint[j]))
            f.write(' '*(max_hint_rows_w-len(str(hint[j]))) + ' ')
        f.write('|')
        for cell in picross[i]:
            f.write(' '*(max_hint_cols_w-len(str(cell))) + ' ')
            f.write('_')
        f.write('\n')
